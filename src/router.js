import ComponentNewVue from '@/components/ComponentNewVue'
import AbouteHotel from '@/components/AbouteHotel.vue'
import ComponentContent from '@/components/ComponentContent'
import { createWebHashHistory } from 'vue-router'
import { createRouter } from 'vue-router'

const routes = [
    {
        path: '/',
        component: ComponentNewVue
    },
    {
        path: '/add',
        component: AbouteHotel
    },
    {
        path: '/content',
        component: ComponentContent
    }
]

const router = createRouter({
    routes,
    history: createWebHashHistory(process.env.BASE_URL)
})

export default router;